New-Item -ItemType Directory -Force -Path C:\Dev\ScriptTestUtility\logs\
C:\Dev\ScriptTestUtility\adfind.exe -b dc=domain,dc=local -f "(objectcategory=person)" > C:\Dev\ScriptTestUtility\logs\trustdomainname_ad_users.txt
C:\Dev\ScriptTestUtility\adfind.exe -b dc=domain,dc=local -f "objectcategory=computer" > C:\Dev\ScriptTestUtility\logs\trustdomainname_ad_computers.txt
C:\Dev\ScriptTestUtility\adfind.exe -b dc=domain,dc=local -f "(objectcategory=organizationalUnit)" > C:\Dev\ScriptTestUtility\logs\trustdomainname_ad_ous.txt
C:\Dev\ScriptTestUtility\adfind.exe -b dc=domain,dc=local -subnets -f "(objectCategory=subnet)"> C:\Dev\ScriptTestUtility\logs\trustdomainname_ad_subnets.txt
C:\Dev\ScriptTestUtility\adfind.exe -b dc=domain,dc=local -f "(objectcategory=group)" > C:\Dev\ScriptTestUtility\logs\trustdomainname_ad_group.txt
C:\Dev\ScriptTestUtility\adfind.exe -b dc=domain,dc=local -gcb -sc trustdmp > C:\Dev\ScriptTestUtility\logs\trustdomainname_ad_trustdmp.txt